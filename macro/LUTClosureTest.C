#include "../src/libRd53b/include/LUT_PlainHMapToColRow.h"

using namespace RD53BDecoding;

void LUTClosureTest(){
  for(unsigned i = 0; i < 65536; i++){
    const uint8_t* array1 = _LUT_PlainHMap_To_ColRow[i];
    // Check whether there are duplicated arrays
    for(unsigned j = i + 1; j < 65536; j++){
      const uint8_t* array2 = _LUT_PlainHMap_To_ColRow[j];
      if(_LUT_PlainHMap_To_ColRow_ArrSize[i] == _LUT_PlainHMap_To_ColRow_ArrSize[j]){
	unsigned sameCount = 0;
	for(unsigned i1 = 0; i1 < _LUT_PlainHMap_To_ColRow_ArrSize[i]; i1++){
	  for(unsigned j1 = 0; j1 < _LUT_PlainHMap_To_ColRow_ArrSize[i]; j1++){
	    if(array1[i1] == array2[j1]) sameCount++;
	  }
	}
	if(_LUT_PlainHMap_To_ColRow_ArrSize[i] > 0 && sameCount >= _LUT_PlainHMap_To_ColRow_ArrSize[i]) cerr <<"Duplicated array!" <<endl;
      }
    }
    // Check whether there are duplicated elements within array
    for(unsigned i1 = 0; i1 < _LUT_PlainHMap_To_ColRow_ArrSize[i]; i1++){
      for(unsigned j1 = i + 1; j1 < _LUT_PlainHMap_To_ColRow_ArrSize[i]; j1++){
	if(array1[i1] == array1[j1]) cerr << "Duplicated element within array!" << endl;
      }
    }
  }
}
