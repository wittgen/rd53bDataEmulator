cmake_minimum_required (VERSION 2.8)

project (rd53bDataEmulator)

# The version number.
set (rd53bDataEmulator_VERSION_MAJOR 1)
set (rd53bDataEmulator_VERSION_MINOR 0)

add_compile_options(-std=c++17)
add_compile_options(-O2)

# Nicer structure for binary files
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

# You need to tell CMake where to find the ROOT installation. This can be done in a number of ways:
#   - ROOT built with classic configure/make use the provided $ROOTSYS/etc/cmake/FindROOT.cmake
#   - ROOT built with CMake. Add in CMAKE_PREFIX_PATH the installation prefix for ROOT
list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})

# require ROOT
find_package( ROOT REQUIRED )
include(${ROOT_USE_FILE})

# Source code
add_subdirectory(RD53BEncoding)
include_directories(RD53BEncoding)

add_subdirectory(RD53BDecoding)
include_directories(RD53BDecoding)

file(GLOB exePaths tool/*)

foreach (_exePath ${exePaths})
	get_filename_component(_exeName ${_exePath} NAME_WE)
	get_filename_component(_exeFile ${_exePath} NAME)
	add_executable( ${_exeName} tool/${_exeFile} )
	target_link_libraries( ${_exeName} ${ROOT_LIBRARIES} RD53BEncoding RD53BDecoding )
endforeach()

set(TARGET_INSTALL_AREA ${PROJECT_SOURCE_DIR})

install(DIRECTORY ${PROJECT_BINARY_DIR}/bin DESTINATION ${TARGET_INSTALL_AREA}  FILES_MATCHING PATTERN "*" PERMISSIONS OWNER_EXECUTE OWNER_WRITE OWNER_READ
    WORLD_EXECUTE WORLD_READ GROUP_EXECUTE GROUP_READ  )
install(DIRECTORY ${PROJECT_BINARY_DIR}/lib DESTINATION ${TARGET_INSTALL_AREA}  FILES_MATCHING PATTERN "*" PERMISSIONS OWNER_EXECUTE OWNER_WRITE OWNER_READ
    WORLD_EXECUTE WORLD_READ GROUP_EXECUTE GROUP_READ  )
