#include "RD53BEncodingTool.h"

#include <bitset>

// Constructor with parameters:
RD53BEncodingTool::RD53BEncodingTool() : m_addresscompression(true),
                                         m_compression(true),
                                         m_suppressToT(false),
                                         m_eventsPerStream(1),
                                         m_suppressEndOfEvent(true),
                                         m_testEvent(0),
                                         m_streamTag(0),
                                         m_intTag(0),
                                         m_testFileName("rd53b"),
                                         m_plainHitMap(false),
                                         m_outputDir("."),
                                         m_debug(false)
{
  m_testStreamsStr.clear();
}

RD53BEncodingTool::StatusCode RD53BEncodingTool::saveDataStream()
{
  addOrphanBits(); // Add orphanBits
  // stream format
  m_testStreamBinFile.open(m_outputDir + "/" + m_testFileName + "_stream.bin", std::ofstream::out | std::ofstream::binary);
  m_testStreamTXTFile.open(m_outputDir + "/" + m_testFileName + "_stream.txt", std::ofstream::out);
  // Split the stream strings into 64-bit blocks
  for (auto stream : m_testStreamsStr)
  {
    int nBlock = stream.length() / 63;
    for (int ib = 0; ib < nBlock; ib++)
    {
      uint64_t output = ((ib == 0 ? 0x1ULL : 0x0ULL) << 63) + std::bitset<63>(stream.substr(ib * 63, 63)).to_ullong();
      m_testStreamBinFile.write((char *)&output, sizeof(uint64_t));
      m_testStreamTXTFile << std::bitset<64>(output) << std::endl;
    }
  }
  m_testChipFile.close();
  m_testStreamBinFile.close();
  m_testStreamTXTFile.close();
  std::cout << "There are " << m_testStreamsStr.size() << " streams processed." << std::endl;
  return RD53BEncodingTool::StatusCode::SUCCESS;
}

RD53BEncodingTool::StatusCode RD53BEncodingTool::initialize()
{
  // TODO: should be taken from the numerology or filled by the user

  // human readable format
  m_testChipFile.open(m_outputDir + "/" + m_testFileName + "_chip.txt", std::ofstream::out);
  // m_testChipFile << "Chip Tag       Pixel Eta       Pixel Phi       cCol        qRow       tot       (bitmap size)     (tot size)      \n";

  return RD53BEncodingTool::StatusCode::SUCCESS;
}

int RD53BEncodingTool::getNumOfOrphanBits(int length, int size)
{
  return (size - length % size) % size;
}

std::string RD53BEncodingTool::getNewTag()
{
  m_testEvent++;
  std::bitset<8> TagX_bitset;
  if (m_eventsPerStream > 1 && m_testEvent % m_eventsPerStream != 1)
  {
    m_intTag++;
    TagX_bitset |= m_intTag;
  }
  else
  {
    m_streamTag++;
    m_intTag = 0;
    TagX_bitset |= m_streamTag;
  }
  return TagX_bitset.to_string();
}

// Each time this function is called, a new event is created
RD53BEncodingTool::StatusCode RD53BEncodingTool::createStream(ChipMap &chipmap)
{
  // ATH_MSG_DEBUG("In RD53BEncodingTool::createStream ... starting!" );

  std::string chipTag = getNewTag();

  // m_testChipFile<< "\nEvent " << std::to_string(m_testEvent) << "\n";

  // Need to start a new stream
  // test Event start from 1
  if (m_eventsPerStream > 1 && m_testEvent % m_eventsPerStream != 1)
  {
    chipTag = "111" + chipTag;
    m_testStreamsStr.back() += chipTag;
  }
  else
  {
    // std::cout << "Starting new stream for event " << m_testEvent << std::endl;
    m_testStreamsStr.push_back(chipTag);
  }

  if (not chipmap.isFilled())
    chipmap.fillRegions();

  if (chipmap.getFiredPixels() == 0)
  {
    // once all this is done, you return
    // ATH_MSG_DEBUG("RD53BEncodingTool::createStream: Empty event");
    return RD53BEncodingTool::SUCCESS;
  }

  // if the chip map is not empty you need to loop on all the fired cores and store the information
  // about the ccol, qrow, bittree, tot
  for (int ccol = 0; ccol < chipmap.getCcols(); ccol++)
  {
    bool isfirst_ccol = true;
    int previous_qrow = -10; // use a default negative value
    int processed_hits = 0;
    for (int qrow = 0; qrow < chipmap.getQrows(); qrow++)
    {

      // -- STEP 1) Construct the data (bitmap+tot)
      // 1) get the bit map length
      // 1.1) if needed, skip the cores without hits
      // 2) get the tot for the selected core

      // 1) get the bit map length
      std::string bitmap_bits = m_plainHitMap ? chipmap.getPlainHitMap(ccol, qrow) : chipmap.getBitTreeString(ccol, qrow, m_compression);
      // 1.1) if needed, skip the cores without hits
      if (bitmap_bits == "")
        continue;

      std::vector<int> tots;

      // 2) get the tot for the selected core
      std::string tot_bits = chipmap.getToTBitsString(ccol, qrow, tots);
      if (m_suppressToT)
        tot_bits = "";

      int min_eta = ccol * chipmap.getCcolsRegion();
      int max_eta = (ccol + 1) * chipmap.getCcolsRegion() - 1;
      int min_phi = qrow * chipmap.getQrowsRegion();
      int max_phi = (qrow + 1) * chipmap.getQrowsRegion() - 1;

      int el = 0;
      bool is_first = true;
      for (int phi = min_phi; phi <= max_phi; phi++)
      {
        for (int eta = min_eta; eta <= max_eta; eta++)
        {
          if (tots.at(el) == 0)
          {
            el++;
            continue;
          }
          if (is_first)
          {
            m_testChipFile << std::bitset<8>(m_streamTag) << " 111" << std::bitset<8>(m_intTag) << " "
                           << eta << " " << phi << " "
                           << ccol << " " << qrow << " " << tots.at(el++) - 1
                           << " (" << bitmap_bits.length() << ") (" << tot_bits.length() << ")\n";
            is_first = false;
          }
          else
            m_testChipFile << std::bitset<8>(m_streamTag) << " 111" << std::bitset<8>(m_intTag) << " "
                           << eta << " " << phi << " "
                           << ccol << " " << qrow << " " << tots.at(el++) - 1 << std::endl;
          processed_hits++;
        }
      }

      // -- STEP 2) Add the tags
      // now you need the ccol, qrow tags + islast_isneighbour:
      // 1) add 2 bits for islast and isneighbour bits
      // 2) if it is the first ccol you need to add the ccol tag
      // 3) add the qrow if the current qrow is not neighbour of previous qrow
      // 4) update the stream

      // 1) add 2 bits for islast and isneighbour bits
      if (m_addresscompression)
      {
        // 2) if it is the first ccol you need to add the ccol tag
        if (isfirst_ccol)
        {
          isfirst_ccol = false;
          std::bitset<6> ccol_bits_string;
          ccol_bits_string |= ccol + 1;
          m_testStreamsStr.back() += ccol_bits_string.to_string();
        }

        std::string islast_isneighbour_qrow_bits = "";
        if (processed_hits == chipmap.getFiredPixelsInCcol(ccol))
          islast_isneighbour_qrow_bits += "1";
        else
        {
          islast_isneighbour_qrow_bits += "0";
        }

        // 3) add the qrow if the current qrow is not neighbour of previous qrow
        if (previous_qrow != (qrow - 1))
        {
          islast_isneighbour_qrow_bits += "0";
          std::bitset<8> qrow_bits_string;
          qrow_bits_string |= qrow; // Qrow index should start from 0
          islast_isneighbour_qrow_bits += qrow_bits_string.to_string();
        }
        else
          islast_isneighbour_qrow_bits += "1";

        // update the previous_row to the current
        previous_qrow = qrow;

        m_testStreamsStr.back() += islast_isneighbour_qrow_bits;
      }
      else
      {
        std::bitset<6> ccol_bits_string;
        ccol_bits_string |= ccol + 1;
        m_testStreamsStr.back() += ccol_bits_string.to_string();

        std::bitset<8> qrow_bits_string;
        qrow_bits_string |= qrow;
        m_testStreamsStr.back() += qrow_bits_string.to_string();
      }

      m_testStreamsStr.back() += (bitmap_bits + tot_bits);
    }
  }

  // ATH_MSG_DEBUG("In RD53BEncodingTool::createStream ... done!" );
  return StatusCode::SUCCESS;
}

int RD53BEncodingTool::addOrphanBits()
{
  // Split the stream strings into 64-bit blocks
  int nBlock = 0;
  for (auto stream : m_testStreamsStr)
  {
    int length = stream.length();
    int orphan_bits = getNumOfOrphanBits(length);
    if (!m_suppressEndOfEvent && orphan_bits < 6)
      orphan_bits += 63;

    if (orphan_bits > 0)
    {
      for (int orp = 0; orp < orphan_bits; orp++)
        stream += "0";
    }

    nBlock += stream.length() / 63;
  }

  return nBlock;
}
