#ifndef RD53BENCODINGTOOL_H
#define RD53BENCODINGTOOL_H

#include "ChipMap_RD53B.h"

#include <iostream>
#include <fstream>

typedef ChipMap_RD53B ChipMap;

class RD53BEncodingTool
{
public:
  RD53BEncodingTool();
  ~RD53BEncodingTool(){};

  enum StatusCode
  {
    FAIL,
    SUCCESS
  };

  StatusCode initialize();
  StatusCode saveDataStream();

  RD53BEncodingTool::StatusCode createStream(ChipMap &chipmap);
  void setEventsPerStream(int NE) { m_eventsPerStream = NE; }
  void setSuppressEndOfEvent(bool flag) { m_suppressEndOfEvent = flag; }
  void setPlainHitMap(bool flag) { m_plainHitMap = flag; }
  void setOutputDir(std::string dir) { m_outputDir = dir; }
  int addOrphanBits();

private:
  void ATH_MSG_DEBUG(const char *string) { std::cout << string << std::endl; }
  void ATH_MSG_INFO(const char *string) { std::cout << string << std::endl; }

  static int getNumOfOrphanBits(int length, int size = 63);
  std::string getNewTag();

  bool m_addresscompression;
  bool m_compression;
  bool m_suppressToT;
  int m_eventsPerStream;
  bool m_suppressEndOfEvent;

  // for testing the stream creation
  unsigned m_testEvent;
  uint8_t m_streamTag;
  uint8_t m_intTag;
  std::string m_testFileName;
  std::vector<std::string> m_testStreamsStr;

  std::ofstream m_testChipFile;
  std::ofstream m_testStreamBinFile;
  std::ofstream m_testStreamTXTFile;
  bool m_plainHitMap;
  std::string m_outputDir;

  bool m_debug;

  // Constants
  static constexpr int NS = 1;
  static constexpr int TagX = 8;
  static constexpr int TagY = 11;
  static constexpr int ccol_bits = 6;
  static constexpr int crow_bits = 8;
  static constexpr int islast_isneighbour = 2;
};

#endif // RD53BENCODINGTOOL_H
