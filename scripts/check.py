#!/usr/bin/env python
import argparse
import os
from collections import OrderedDict

################# Main function #################
parser = argparse.ArgumentParser()

occupancyArr = OrderedDict([("1e-5", 100000), ("1e-4", 50000), ("1e-3", 10000), ("1e-2", 5000)])
for orientation in [0, 1]:
    for occ, nevt in occupancyArr.items():
	for clusterSize in [1, 2, 3, 5, 10]:
            print("Cluster size: {}, Orientation: {}, Occupancy: {}, Nevt: {}".format(clusterSize, orientation, occ, nevt))
            for plainHitMap in [0, 1]:
	        outputDir = "output/{}_{}_{}_{}".format(clusterSize, orientation, occ, plainHitMap)
                summaryFileName = outputDir+"/summary.txt"
                streamFileName = outputDir+"/rd53b_stream.txt"
                xcheckFileName = outputDir+"/rd53b_chip.txt"

                summary = open(summaryFileName, "r").readlines()
                streamCnt = len(open(streamFileName).readlines())
                hitCnt = len(open(xcheckFileName).readlines())

                summaryArr = summary[1].split()

                if int(summaryArr[0]) != streamCnt*2 or int(summaryArr[1]) != hitCnt:
                    print("{} has mismatch!".format(outputDir))
