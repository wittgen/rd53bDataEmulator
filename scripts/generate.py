#!/usr/bin/env python
import argparse
import os
from collections import OrderedDict

################# Main function #################
parser = argparse.ArgumentParser()
args = parser.parse_args()

#occupancyArr = OrderedDict([("1e-5", 100000), ("1e-4", 50000), ("1e-3", 10000), ("1e-2", 5000)])
occupancyArr = OrderedDict([("1e-6", 10000), ("5e-6", 10000), ("2e-5", 10000), ("4e-5", 10000), ("6e-5", 10000), ("8e-5", 10000)])

for orientation in [0, 1]:
    for occ, nevt in occupancyArr.items():
	for clusterSize in [1, 2, 3, 5, 10]:
            print("Cluster size: {}, Orientation: {}, Occupancy: {}, Nevt: {}".format(clusterSize, orientation, occ, nevt))
            for plainHitMap in [0, 1]:
                outputDir="output/{}_{}_{}_{}".format(clusterSize, orientation, occ, plainHitMap)
                os.system("mkdir -vp {}".format(outputDir))
	        os.system("./bin/encoder -t {} -n 1 -o {} -c {} -r {} -p {} -d {} &".format(nevt, float(occ)/float(clusterSize), clusterSize, orientation, plainHitMap, outputDir))
