#!/usr/bin/env python
import argparse
import os
from collections import OrderedDict

################# Main function #################
parser = argparse.ArgumentParser()

parser.add_argument('--enableEncoding', '-e', type=int, required=False, default=0, help="Enable encoding")
parser.add_argument('--plainHitMap', '-p', type=int, required=False, default=0, help="Use plain hit map")

args = parser.parse_args()

occupancyArr = OrderedDict([("1e-5", 100000), ("1e-4", 50000), ("1e-3", 10000), ("1e-2", 5000)])

for orientation in [0, 1]:
    for occ, nevt in occupancyArr.items():
	for clusterSize in [1, 2, 3, 5, 10]:
            print("Cluster size: {}, Orientation: {}, Occupancy: {}, Nevt: {}".format(clusterSize, orientation, occ, nevt))
#            for plainHitMap in [0, 1]:
            outputDir="output/{}_{}_{}_{}".format(clusterSize, orientation, occ, args.plainHitMap)
            if args.enableEncoding == 1:
	        os.system("./bin/encoder -t {} -n 1 -o {} -c {} -r {} -p {}".format(nevt, float(occ)/float(clusterSize), clusterSize, orientation, args.plainHitMap))
	        os.system("mkdir -vp {}".format(outputDir))
                os.system("mv rd53b_stream.txt rd53b_chip.txt {}".format(outputDir))
                    
	    os.system("./bin/decoder -r 100 -s {}/rd53b_stream.txt -x {}/rd53b_chip.txt -o {}/summary.txt".format(outputDir, outputDir, outputDir))


            
