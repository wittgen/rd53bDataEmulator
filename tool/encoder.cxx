#include "RD53BEncoding/ChipMap_RD53B.h"
#include "RD53BEncoding/RD53BEncodingTool.h"

#include <getopt.h>
#include <memory>

using namespace std;

struct option longopts[] = {
    {"total", required_argument, NULL, 't'},
    {"nEvtPerStream", required_argument, NULL, 'n'},
    {"occupancy", required_argument, NULL, 'o'},
    {"clustersize", required_argument, NULL, 'c'},
    {"orientation", required_argument, NULL, 'r'},
    {"plain", required_argument, NULL, 'p'},
    {"dir", required_argument, NULL, 'd'},
    {"help", no_argument, NULL, 'h'},
    {0, 0, 0, 0}};

void printHelp(std::string exe)
{
  cout << "Usage: " << exe << " [options]" << endl;
  cout << "Allowed options:" << endl;
  cout << " -t [ --total ] arg                 Total number of events to be generated (default 1)" << endl;
  cout << " -n [ --nEvtPerStream ] arg         Number of events in each stream (default 1)" << endl;
  cout << " -o [ --occupancy ] arg             Occupancy (default 1e-3)" << endl;
  cout << " -c [ --clusterSize ] arg           Size of cluster (default 2)" << endl;
  cout << " -r [ --orientation ] arg           Cluster orientation, choose between eta (0) and phi (1) (default 0)" << endl;
  cout << " -p [ --plainHitMap ] arg           Whether to generate plain hit map (default false)" << endl;
  cout << " -d [ --dir ] arg                   Output dir (default pwd)" << endl;
  cout << " -h [ --help ]                      Produce help message" << endl;
}

int main(int argc, char **argv)
{
  // if( argc < 2 ){
  // 	printf("Usage: %s <n event> <events per stream> \n", argv[0]);
  // 	return 0;
  // }
  int N_total = 1;
  int N_E = 1;
  double occupancy = 1e-3;
  int clusterSize = 1;
  int orientation = 0;
  bool plainHitMap = false;
  std::string outputDir = ".";
  int oc;
  while ((oc = getopt_long(argc, argv, "t:n:o:c:r:p:d:h", longopts, NULL)) != -1)
  {
    switch (oc)
    {
    case 't':
      N_total = atoi(optarg);
      cout << "Set total number of events to " << N_total << endl;
      break;
    case 'n':
      N_E = atoi(optarg);
      cout << "Set number of events per stream to " << N_E << endl;
      break;
    case 'o':
      occupancy = atof(optarg);
      cout << "Set occupancy to " << occupancy << endl;
      break;
    case 'c':
      clusterSize = atoi(optarg);
      cout << "Set cluster size to " << clusterSize << endl;
      break;
    case 'r':
      orientation = atoi(optarg);
      cout << "Set orientation to " << orientation << endl;
      break;
    case 'p':
      plainHitMap = atoi(optarg);
      cout << "Set generate plain hit map to " << plainHitMap << endl;
      break;
    case 'd':
      outputDir = optarg;
      cout << "Set output dir to " << outputDir << endl;
      break;
    case 'h':
      printHelp(argv[0]);
      return 0;
    case ':': /* missing option argument */
      fprintf(stderr, "%s: option `-%c' requires an argument\n",
              argv[0], optopt);
      printHelp(argv[0]);
      return 0;
    case '?':
    default:
      fprintf(stderr, "%s: option `-%c' is invalid: ignored\n",
              argv[0], optopt);
      printHelp(argv[0]);
      return 0;
    }
  }

  RD53BEncodingTool *tool = new RD53BEncodingTool();
  tool->setEventsPerStream(N_E);
  tool->setPlainHitMap(plainHitMap);
  tool->setOutputDir(outputDir);
  tool->initialize();

  unique_ptr<ChipMap_RD53B> map;

  for (int ievt = 0; ievt < N_total; ievt++)
  {
    // map->readMapFile(inputMapFileName);
    // Read in data pattern and load chip map
    // RD53B chip geometry
    map.reset(new ChipMap_RD53B(400, 384, 8, 2));
    map->generateRndmEvent(occupancy, clusterSize, orientation, ievt); // Generate random hits
    tool->createStream(*map);
  }
  tool->saveDataStream();
}
