#include "RD53BEncoding/ChipMap_RD53B.h"
#include "RD53BEncoding/RD53BEncodingTool.h"

#include <getopt.h>
#include <memory>

using namespace std;

#define NCOL 400
#define NROW 384
#define NCOL_CCOL 8
#define NROW_QROW 2

enum maskStagingType
{
    StdMask = 0,
    PToTMask = 1
};

maskStagingType maskType = StdMask;
unsigned N_step = 64;

bool applyMask(unsigned col, unsigned row, int m_cur)
{
    // This is the mask pattern
    unsigned core_row = row / 8;
    unsigned serial;
    if (maskType == PToTMask)
    {
        serial = row * 2 + (col % 8) / 4;
    }
    else
    {
        serial = (core_row * 64) + ((col + (core_row % 8)) % 8) * 8 + row % 8;
    }

    if ((serial % N_step) == m_cur)
    {
        return true;
    }
    return false;
}

struct option longopts[] = {
    {"triggerFrequency", required_argument, NULL, 't'},
    {"nEvtPerStream", required_argument, NULL, 'n'},
    {"ccol", required_argument, NULL, 'c'},
    {"plain", required_argument, NULL, 'p'},
    {"dir", required_argument, NULL, 'd'},
    {"maskStaging", required_argument, NULL, 'm'},
    {"baseline", required_argument, NULL, 'b'},
    {"help", no_argument, NULL, 'h'},
    {0, 0, 0, 0}};

void printHelp(std::string exe)
{
    cout << "Usage: " << exe << " [options]" << endl;
    cout << "Allowed options:" << endl;
    cout << " -t [ --triggerFrequency ] arg      Trigger frequency in Hz (default 5000)" << endl;
    cout << " -n [ --nEvtPerStream ] arg         Number of events in each stream (default 1)" << endl;
    cout << " -c [ --ccol ] arg                  Number of core columns per step (default 2)" << endl;
    cout << " -p [ --plainHitMap ] arg           Whether to generate plain hit map (default false)" << endl;
    cout << " -d [ --dir ] arg                   Output dir (default pwd)" << endl;
    cout << " -m [ --maskStaging ] arg           Mask staging pattern code (default 0)" << endl;
    cout << " -b [ --baseline ] arg              Baseline bandwidth in Gpbs (default 1.28)" << endl;
    cout << " -h [ --help ]                      Produce help message" << endl;
}

int main(int argc, char **argv)
{
    int maskStaging = StdMask;
    int N_E = 1;
    int N_ccol = 2;
    double triggerFrequency = 5000;
    double baseline = 1.28;
    bool plainHitMap = false;
    std::string outputDir = ".";
    int oc;
    while ((oc = getopt_long(argc, argv, "t:n:c:p:d:m:b:h", longopts, NULL)) != -1)
    {
        switch (oc)
        {
        case 't':
            triggerFrequency = atof(optarg);
            cout << "Set trigger frequency to " << triggerFrequency << " Hz" << endl;
            break;
        case 'b':
            baseline = atof(optarg);
            cout << "Set baseline band width to " << baseline << " Gbps" << endl;
            break;            
        case 'n':
            N_E = atoi(optarg);
            cout << "Set number of events per stream to " << N_E << endl;
            break;
        case 'c':
            N_ccol = atoi(optarg);
            cout << "Set number of core columns scan steps to " << N_ccol << endl;
            break;
        case 'p':
            plainHitMap = atoi(optarg);
            cout << "Set generate plain hit map to " << plainHitMap << endl;
            break;
        case 'd':
            outputDir = optarg;
            cout << "Set output dir to " << outputDir << endl;
            break;
        case 'm':
            maskStaging = atoi(optarg);
            cout << "Set mask staging pattern code to " << maskStaging << endl;
            break;
        case 'h':
            printHelp(argv[0]);
            return 0;
        case ':': /* missing option argument */
            fprintf(stderr, "%s: option `-%c' requires an argument\n",
                    argv[0], optopt);
            printHelp(argv[0]);
            return 0;
        case '?':
        default:
            fprintf(stderr, "%s: option `-%c' is invalid: ignored\n",
                    argv[0], optopt);
            printHelp(argv[0]);
            return 0;
        }
    }

    RD53BEncodingTool *tool = new RD53BEncodingTool();
    tool->setEventsPerStream(N_E);
    tool->setPlainHitMap(plainHitMap);
    tool->setOutputDir(outputDir);
    tool->initialize();

    int N_step = 0;
    if (maskStaging == StdMask)
    {
        N_step = 64;
    }
    else if (maskStaging == PToTMask)
    {
        N_step = 768;
    }

    unique_ptr<ChipMap_RD53B> map;

    for (int istep = 0; istep < 1; istep++)
    {
        map.reset(new ChipMap_RD53B(NCOL, NROW, NCOL_CCOL, NROW_QROW));
        for (int iccol = 0; iccol < 1; iccol++)
        {
            for (int col = 0; col < NCOL; col++)
            {
                int ccol = col / 8;
                if (ccol % N_ccol != iccol)
                    continue;
                for (int row = 0; row < NROW; row++)
                {
                    if (!applyMask(col, row, istep))
                        continue;
                    map->fillChipMap(col, row, 7);
                }
            }
        }
        tool->createStream(*map);
    }
    int nTrigger = 16;
    int nBlock = tool->addOrphanBits() + (nTrigger - 1);
    double bandWidth = (nBlock * 66.) * triggerFrequency / 1e9;
    cout << "-------------------- SUMMARY --------------------"<<endl;
    cout << "Trigger rate is " << triggerFrequency << " Hz" << endl;
    cout << "Number of steps to scan core column is " << N_ccol << endl;
    cout << "Baseline bandwidth is " << baseline << " Gpbs" << endl;
    cout << "Band width is " << bandWidth << " Gbps, " << bandWidth / baseline * 100 << "% of total band width" << endl;
    tool->saveDataStream();
}
