#ifndef RD53BDATAPROCESSOR_H
#define RD53BDATAPROCESSOR_H

#include <vector>
#include <map>
#include <fstream>
#include <chrono>
#include <memory>
#include <iomanip>

#include "LoopStatus.h"
#include "Fei4EventData.h"

#define USE64 0			// Use uint64_t or uint32_t
#define MAKEEVENT 0		// Make event (Fei4Event) or not
#define XCHECK 0		// When swtich on XCHECK, please switch on MAKEEVENT as well. Otherwise the code will crash
#define PLAINHITMAP 0		// Use plain hitmap or not

#define BINARYTREE_DEPTH 4
#define BLOCKSIZE 64
#define HALFBLOCKSIZE 32

namespace RD53BDecoding{
  class RawData {
  public:
#if USE64 == 1
    RawData(uint32_t arg_adr, uint64_t *arg_buf, unsigned arg_words){
      adr = arg_adr;
      buf = arg_buf;
      words = arg_words;
    }
    uint64_t *buf;
#else
    RawData(uint32_t arg_adr, uint32_t *arg_buf, unsigned arg_words){
      adr = arg_adr;
      buf = arg_buf;
      words = arg_words;
    }
    uint32_t *buf;
#endif
    ~RawData(){};
    
    uint32_t adr;
    unsigned words;
  };

  class RawDataContainer {
  public:
    RawDataContainer(LoopStatus &&s) : stat(s) {}
    ~RawDataContainer() {
      for(unsigned int i=0; i<adr.size(); i++)
	delete[] buf[i];
    }

    void add(RawData *d) {
      adr.push_back(d->adr);
      buf.push_back(d->buf);
      words.push_back(d->words);
      delete d;
    }

    unsigned size() {
      return adr.size();
    }

    std::vector<uint32_t> adr;
#if USE64 == 1
    std::vector<uint64_t*> buf;
#else
    std::vector<uint32_t*> buf;
#endif
    std::vector<unsigned> words;
    LoopStatus stat;
  };

  class Rd53bDataProcessor {
  public:
    Rd53bDataProcessor(uint16_t ncol, uint16_t nrow);
    ~Rd53bDataProcessor(){};

    double process();
    int readInData(std::string inputStreamFileName, std::string inputXCheckFileName, int nStream = -1);
    int finalize();
    void setDebug(int level){_debug = level;}
    void setPlainHitMap(bool flag){_plainHitMap = flag;}
    void reset(const bool clear = false);
  private:
    // YARR input/output data format
    std::unique_ptr<RawData> m_curIn;
#if MAKEEVENT == 1 || MAKEEVENT == 3
    std::unique_ptr<Fei4Data> m_curOut;
#elif MAKEEVENT == 2
    uint16_t m_curOut[100000][200000][3];
#endif
    uint64_t m_nEvent, m_nHit[100000];

    // Data stream bookkeeping
#if USE64 == 1
    const uint64_t *_data;	// Pointer to one data block
#else
    const uint32_t *_data;	// Pointer to one data block
#endif
    
    int _blockIdx;		// Index of the data block
    int _bitIdx;			// Index of the first bit in datablock which is not processed yet. It starts from 0. The first half thus ends at 31, and the 2nd starts at 32
#if USE64 == 1
    std::vector<uint64_t> _buffer; // Array for holding input data streams
#else
    std::vector<uint32_t> _buffer; // Array for holding input data streams
#endif

    // Inline functions frequently used
    inline uint64_t retrieve(const unsigned length, const bool checkEOS = false); // Retrieve bit string with length
    inline void startNewStream(); // Start a new stream (read tag, L1ID, BCID)
    inline void rollBack(const unsigned length); // Roll back bit index
    inline void processData();
#if PLAINHITMAP == 0
    inline uint8_t getBitPair(); // Get decoded bit pair used in hit map
#endif
    // For debugging only
    std::vector<std::tuple<uint16_t, uint16_t, uint8_t> > _xcheckMap;

    int _debug;
    bool _plainHitMap;
    unsigned _cannotBeFound;
    unsigned _wrongToT;
  };

}
#endif
