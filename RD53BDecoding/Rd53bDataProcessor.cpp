#include <iostream>
#include <sstream>
#include <algorithm>
#include <bitset>

#include "Rd53bDataProcessor.h"
#include "LUT_PlainHMapToColRow.h"

#if PLAINHITMAP == 0
#include "LUT_BinaryTreeRowHMap.h"
#include "LUT_BinaryTreeHitMap.h"
#endif

#define HEXF(x, y) std::hex << "0x" << std::hex << std::setw(x) << std::setfill('0') << (y) << std::dec
#define likely(x)       __builtin_expect((x),1)
#define unlikely(x)     __builtin_expect((x),0)

typedef unsigned __int128 uint128_t;

namespace RD53BDecoding{
  
  Rd53bDataProcessor::Rd53bDataProcessor(uint16_t ncol, uint16_t nrow){
    reset();
    _debug = 0;
    _plainHitMap = false;
  }

  int Rd53bDataProcessor::finalize(){
    // Summarize
    int badEvtCnt = 0;
    int nHits = 0;
#if MAKEEVENT == 1 || MAKEEVENT == 3
    std::cout << "Number of events processed: " << m_curOut->nEvents << std::endl;
    for(int ievt = 0; ievt < m_curOut->nEvents; ievt++) nHits += m_curOut->events[ievt].nHits;
    std::cout << "Number of hits processed: " << nHits << std::endl;
#else
    std::cout << "Number of events processed: " << m_nEvent << std::endl;
    for(int ievt = 0; ievt < m_nEvent; ievt++) nHits += m_nHit[ievt];
    std::cout << "Number of hits processed: " << nHits << std::endl;
#endif
#if XCHECK == 1
    if(_cannotBeFound > 0){
      std::cout << _cannotBeFound << " events cannot be found in x-check data" << std::endl;
    }
    if(_wrongToT > 0){
      std::cout << _wrongToT << " events can be mactched to x-check data but have wrong ToT" << std::endl;
    }

    for(unsigned ievt = 0; ievt < _xcheckMap.size(); ievt++){
      badEvtCnt++;
    }
#endif
    if(badEvtCnt == 0) std::cout << std::endl << "*** All the hits are processed correctly. Congratulations! ***" << std::endl << std::endl;
    else std::cout << std::endl << "*** " << badEvtCnt << " hits are not fully matched ***" << std::endl << std::endl;
    return nHits;
  }

  inline uint64_t Rd53bDataProcessor::retrieve(const unsigned length, const bool checkEOS){
#if XCHECK == 1
    if(_debug >= 2) std::cout << __PRETTY_FUNCTION__ << ": block idx = " << _blockIdx << " bit idx = " << _bitIdx << " length = " << length << std::endl;
#endif
    if(unlikely(length == 0)) return 0;
#if USE64 == 1
    if(checkEOS && (_data[0] >> 63) && _bitIdx == 1){ // Corner case where end of event mark (0000000) is suppressed and there is no orphan bits
      _blockIdx--;		  // Roll back block index by 1. A new stream will start in the next loop iteration
      return 0;
    }

    uint64_t variable = 0;
    //const uint64_t mask = 0xFFFFFFFFFFFFFFFF;

    if(_bitIdx + length < BLOCKSIZE){ // Need to read in next block
      variable = ((_data[0] & (0xFFFFFFFFFFFFFFFFUL >> _bitIdx)) >> (BLOCKSIZE - length - _bitIdx));
      _bitIdx += length; // Move bit index
    }
    else{
      if(checkEOS && (_data[1] >> 63)){ // Check end of stream
	return 0;
      }
      // Read the remaining of the block
      variable = ((_data[0] & (uint128_t(0xFFFFFFFFFFFFFFFF) >> _bitIdx)) << (length + _bitIdx - BLOCKSIZE)) | ((_data[1] & 0x7FFFFFFFFFFFFFFFUL) >> (127 & ~(length + _bitIdx)));
      _data = &_data[1];
      ++_blockIdx; // Increase block index 
      _bitIdx -= (63 - length); // Reset bit index. Since we always read the NS bit the index should always start from 1

#if XCHECK == 1
      if(_debug >= 2) std::cout << "Reading next block: " << std::bitset<64>(_data[0]) << std::endl;
#endif
    }
#else
    if(checkEOS && (_data[0] >> 31) && _bitIdx == 1){ // Corner case where end of event mark (0000000) is suppressed and there is no orphan bits
      _blockIdx--;		  // Roll back block index by 1. A new stream will start in the next loop iteration
      return 0;
    }
    uint64_t variable = 0;

    if(_bitIdx + length < BLOCKSIZE){ // Need to read in next block
      variable = (((_bitIdx + length) <= HALFBLOCKSIZE) || (_bitIdx >= HALFBLOCKSIZE)) ?
	((_data[_bitIdx/HALFBLOCKSIZE] & (0xFFFFFFFFUL >> (_bitIdx - ((_bitIdx >> 5) << 5)))) >> ((((_bitIdx >> 5) + 1) << 5) - length - _bitIdx))
	: (((_data[0] & (0xFFFFFFFFUL >> _bitIdx)) << (length + _bitIdx - HALFBLOCKSIZE)) | (_data[1] >> (BLOCKSIZE - length - _bitIdx)));
      _bitIdx += length; // Move bit index
    }
    else{
      if(checkEOS && (_data[2] >> 31)){ // Check end of stream
	return 0;
      }

      variable = (((_bitIdx < HALFBLOCKSIZE) ? (((_data[0] & (0xFFFFFFFFUL >> _bitIdx)) << HALFBLOCKSIZE) | _data[1]) : (_data[1] & (0xFFFFFFFFUL >> (_bitIdx - HALFBLOCKSIZE)))) << (length + _bitIdx - BLOCKSIZE)) |
	(((_bitIdx + length) < (HALFBLOCKSIZE + BLOCKSIZE)) ? ((_data[2] & 0x7FFFFFFFUL) >> (0x5F & ~(_bitIdx + length))) : (((_data[2] & 0x7FFFFFFFUL) << (_bitIdx + length - 0x5F)) | (_data[3] >> (0x7F & ~(length + _bitIdx)))));

      ++_blockIdx; // Increase block index
      _data = &_data[2];

      _bitIdx -= (63 - length); // Reset bit index. Since we always read the NS bit the index should always start from 1

#if XCHECK == 1
      if(_debug >= 2) std::cout << "Reading next block: " << std::bitset<32>(_data[0]) << std::bitset<32>(_data[1]) << std::endl;
#endif
    }
#endif

#if XCHECK == 1
    if(_debug >= 2) std::cout << "Retrieve " << length << " bits with result: " << HEXF((length + 3)/4, variable) << std::endl; //
#endif
    return variable;
  }
  
#if PLAINHITMAP == 0
  inline uint8_t Rd53bDataProcessor::getBitPair(){
    // if(_debug >= 3) std::cout << __PRETTY_FUNCTION__ << std::endl;
    const uint8_t bitpair = retrieve(2);
    // There is only 11 and 10, and 01 is replaced with 0
    // Whenever 00 or 01 show up, replace the first 0 with 01, and move 1-bit back
    if(bitpair == 0x0 || bitpair == 0x1){
      rollBack(1); // Moving bit index back by 1
      return 0x1;
    }
    return bitpair;
  }
#endif
  
  double Rd53bDataProcessor::process(){
    // std::cout << __PRETTY_FUNCTION__ << std::endl;
    std::cout << "There are " << m_curIn->words/2 << " blocks in total." << std::endl;

    auto start = std::chrono::steady_clock::now();
    processData();
    auto end = std::chrono::steady_clock::now();
    std::chrono::duration<double> diff = end-start;
    std::cout << "Time to finish processing all events: " << diff.count() << " s\n";
    return diff.count();
    // std::cout << "There are " << _blockIdx << " blocks from " << _events<< " events in " << _streams << " streams processed. " << _hits << " hits recorded." << std::endl;
  }

  inline void Rd53bDataProcessor::rollBack(const unsigned length){
#if XCHECK == 1
    if(_debug >= 2) std::cout << __PRETTY_FUNCTION__ << ": Current block idx = " << _blockIdx << ", bit idx = " << _bitIdx << ", length = " << int(length) << std::endl;
#endif
    if(unlikely(length == 0)) return;
    if(_bitIdx >= (length + 1)) _bitIdx -= length; // Keep in mind there is one extra bit from NS
    else{ // Across block, roll back by length
      // if(_debug >= 2) std::cout << "Rolling back to previous block. Current block idx = " << _blockIdx << ", bit idx = " << _bitIdx << ", length = " << int(length) << std::endl;
      _bitIdx += (63 & ~length);
#if USE64 == 1
      _data = &m_curIn->buf[--_blockIdx - 1];
#else
      _data = &m_curIn->buf[2 * (--_blockIdx - 1)];
#endif
      // if(_debug) std::cout << std::bitset<32>(_data[0]) << std::bitset<32>(_data[1]) << std::endl; //
    }
  }

  int Rd53bDataProcessor::readInData(std::string inputStreamFileName, std::string inputXCheckFileName, int nStream){
    std::cout << "Reading encoded data...";
    uint32_t address = 0xFFFFFFFF;
    unsigned words = 0;
    std::ifstream fstream(inputStreamFileName);
    std::string line;
    int streamCnt=0;
    while((std::getline(fstream, line))){
#if USE64 == 1
      std::bitset<64> block(line);
      uint64_t data = block.to_ulong();
      _buffer.push_back(data);
      if(((data >> 63) & 0x1)) streamCnt++;
#else
      uint32_t data1 = std::bitset<32>(line.substr(0, 32)).to_ulong();
      uint32_t data2 = std::bitset<32>(line.substr(32, 32)).to_ulong();
      _buffer.push_back(data1);
      _buffer.push_back(data2);
      if(((data1 >> 31) & 0x1)) streamCnt++;
#endif
      if(nStream > 0 && streamCnt > nStream) break;
      words+=2;
      // std::cout << HEXF(8, data1) << " " << HEXF(8, data2) << std::endl;
    }
    fstream.close();
#if USE64 == 0
    _buffer.push_back(0);
#endif
    _buffer.push_back(0);
    m_curIn.reset(new RawData(address, &_buffer[0], words));

    // if(_debug >= 1){
#if XCHECK == 1
    std::ifstream fxcheck(inputXCheckFileName);
    std::bitset<11> tag, intTag, pre_tag, pre_intTag;
    uint16_t col, row, ccol, qrow, tot;
    int hitmaps = 0, hits = 0, events = 0;
    while((std::getline(fxcheck, line))){
      if(_debug >= 4) std::cout << line << std::endl;
      std::stringstream ss(line);
      ss >> tag >> intTag >> col >> row >> ccol >> qrow >> tot;
      if(_debug >= 3) std::cout << "Tag: " << tag << ", int tag: " << intTag << ", Col: " << col << ", Row: " << row << ", ToT: " << int(tot) << std::endl;
      if(pre_tag != tag || pre_intTag != intTag){
      // 	std::vector<std::tuple<uint16_t, uint16_t, uint8_t> > temp_map;
      // 	_xcheckMap.push_back(temp_map);
      	pre_tag = tag;
      	pre_intTag = intTag;
	events++;
      }
      _xcheckMap.push_back(std::make_tuple(col, row, tot));
      if(line.find("(") != std::string::npos) hitmaps++;
      hits++;
    }

    fxcheck.close();
    std::cout << __PRETTY_FUNCTION__ << ": There are " << events << " events and " << hits << " hits read in total" <<std::endl;
#endif
    // }

    std::cout << "Done." << std::endl;
    return words;
  }

  inline void Rd53bDataProcessor::startNewStream(){
#if USE64 == 1
    _data = &m_curIn->buf[_blockIdx];
    const uint8_t tag = (*_data >> 55) & 0xFF;
#else
    _data = &m_curIn->buf[2 * _blockIdx];
    const uint8_t tag = (_data[0] >> 23) & 0xFF;
    // if(_debug >= 2) std::cout << __PRETTY_FUNCTION__ << ": " << std::bitset<32>(_data[0]) << std::bitset<32>(_data[1]) << std::endl;
#endif
    ++_blockIdx; // Increase block index
    _bitIdx = 9; // Reset bit index = NS + tag
    // if(_debug) std::cout << "New tag: " << HEXF(8, _tag) << std::endl;
    // _intTag = 0x700; // The first internal tag equals to 11100000000
    const uint16_t l1id = 0xFF; // For now assign dummy value
    const uint16_t bcid = 0x7FF; // For now assign dummy value
#if MAKEEVENT == 0
    m_nEvent++;
#elif MAKEEVENT == 1
    m_curOut->newEvent(tag, l1id, bcid);
#elif MAKEEVENT == 2
    m_nEvent++;
    m_curOut[m_nEvent - 1][0][0] = tag;
    m_curOut[m_nEvent - 1][0][1] = l1id;
    m_curOut[m_nEvent - 1][0][2] = l1id;
#elif MAKEEVENT == 3
    m_curOut->newEventReserve(tag, l1id, bcid);
    m_curOut->curEvent->reserve(400*384*0.1);
#endif
  }

  void Rd53bDataProcessor::reset(bool clear){
    // if(_debug >= 1) std::cout << __PRETTY_FUNCTION__ << std::endl;
    _blockIdx = 0; // Index of block, starting from 0
    _bitIdx = 0; // Index of bit within block, starting from 0
    _data = nullptr;
#if MAKEEVENT == 1 || MAKEEVENT == 3
    if(clear){
      for(auto event : m_curOut->events) event.hits.clear();
      m_curOut->events.clear();
    }
    m_curOut.reset(new Fei4Data());
#endif
    for(int ievt = 0; ievt < m_nEvent; ievt++) m_nHit[ievt] = 0;
    m_nEvent = 0;
    _cannotBeFound = 0;
    _wrongToT = 0;
  }

  void Rd53bDataProcessor::processData(){
    const unsigned blocks = m_curIn->words/2;
#if MAKEEVENT == 3
    m_curOut->reserve(m_curIn->words);
#endif
    startNewStream();
    uint8_t ccol = 0, qrow = 0;

    while(_blockIdx <= blocks){
      // Start from getting core column
      ccol = retrieve(6, true);
      // if(_debug) std::cout << "Column number: " << HEXF(6, ccol) << std::endl;
      // End of stream marked with 000000 in current stream
      if(ccol == 0){
	if(unlikely(_blockIdx >= blocks)) break; // End of data processing
	startNewStream();
	continue;
      }
      else if(ccol >= 0x38){ // Internal tag
	const uint16_t intTag = (ccol << 5) | retrieve(5);
	const uint16_t l1id = 0xFF; // For now assign dummy value
	const uint16_t bcid = 0x7FF; // For now assign dummy value
#if MAKEEVENT == 0
	m_nEvent++;
#elif MAKEEVENT == 1
	m_curOut->newEvent(intTag, l1id, bcid);
#elif MAKEEVENT == 2
	m_nEvent++;
	m_curOut[m_nEvent - 1][0][0] = intTag;
	m_curOut[m_nEvent - 1][0][1] = l1id;
	m_curOut[m_nEvent - 1][0][2] = bcid;
#elif MAKEEVENT == 3
	m_curOut->newEventReserve(intTag, l1id, bcid);
	m_curOut->curEvent->reserve(400*384*0.1);
#endif
	continue;
      }

      // Loop over all the hits
      uint32_t islast_isneighbor_qrow_hmap = 0;
      do{
	islast_isneighbor_qrow_hmap = retrieve(10);
#if XCHECK == 1
	if(_debug >= 2) std::cout << "islast = " << bool(islast_isneighbor_qrow_hmap & 0x200) << ", isneighbor = " << bool(islast_isneighbor_qrow_hmap & 0x100) << std::endl;
#endif
	if(islast_isneighbor_qrow_hmap & 0x100){
	  ++qrow;
	  rollBack(8);
	}
	else{
	  qrow = islast_isneighbor_qrow_hmap & 0xFF;
	}

#if PLAINHITMAP == 1
	const uint16_t hitmap = retrieve(16);
#else
	// First read 16-bit, then see whether it is enough
	const uint16_t hitmap_raw = retrieve(16);
	uint16_t hitmap = (_LUT_BinaryTreeHitMap[hitmap_raw] & 0xFFFF);
	// std::cout<< HEXF(4, hitmap_raw) << " " << HEXF(8, _LUT_BinaryTreeHitMap[hitmap_raw])<<std::endl;
	const uint8_t hitmap_rollBack = ((_LUT_BinaryTreeHitMap[hitmap_raw] & 0xFF000000) >> 24);
	if(hitmap_rollBack > 0){ // Rollback and read the second row
	  if(hitmap_rollBack != 0xff) rollBack(hitmap_rollBack);
	  const uint16_t rowMap = retrieve(14);
	  hitmap |= (_LUT_BinaryTreeRowHMap[rowMap] << 8);
	  rollBack((_LUT_BinaryTreeRowHMap[rowMap] & 0xFF00) >> 8);
	}
	else{
	  rollBack((_LUT_BinaryTreeHitMap[hitmap_raw] & 0xFF0000) >> 16);
	}
	
	// ++++++++++++++++++++ Read row-by-row ++++++++++++++++++++
	// uint16_t hitmap = 0x0; // Start from empty
	// uint8_t root = getBitPair();

	// if(root & 0x2){		// Read the first row
	//   const uint16_t rowMap = retrieve(14);
	//   hitmap = (_LUT_BinaryTreeRowHMap[rowMap] & 0x00FF);
	//   rollBack((_LUT_BinaryTreeRowHMap[rowMap] & 0xFF00) >> 8);
	// }
	// if(root & 0x1){
	//   const uint16_t rowMap = retrieve(14);
	//   hitmap |= (_LUT_BinaryTreeRowHMap[rowMap] << 8);
	//   rollBack((_LUT_BinaryTreeRowHMap[rowMap] & 0xFF00) >> 8);
	// }

#if XCHECK == 1
	if(_debug >= 2)  std::cout << "Final hit map " << HEXF(4, hitmap)
				     << std::endl;
#endif

#endif	

#if XCHECK == 1
	if(_debug >= 2) std::cout<< "There are " << int(_LUT_PlainHMap_To_ColRow_ArrSize[hitmap]) << " hits in the qrow " << int(qrow) <<std::endl;
#endif
	const uint64_t ToT = retrieve((_LUT_PlainHMap_To_ColRow_ArrSize[hitmap] << 2));
	for(int ihit = 0; ihit < _LUT_PlainHMap_To_ColRow_ArrSize[hitmap]; ++ihit){
	  const uint8_t pix_tot = (ToT >> (ihit << 2)) & 0xF;
	//   uint8_t pix_tot = retrieve(4);
	  const uint16_t pix_col = (ccol - 1) * 8 + (_LUT_PlainHMap_To_ColRow[hitmap][ihit] >> 4);
	  const uint16_t pix_row = (qrow) * 2 + (_LUT_PlainHMap_To_ColRow[hitmap][ihit] & 0xF);
#if MAKEEVENT == 0
	  m_nHit[m_nEvent - 1]++;
#elif MAKEEVENT == 1
	  m_curOut->curEvent->addHit(pix_row, pix_col, pix_tot);
#elif MAKEEVENT == 2
	  m_nHit[m_nEvent - 1]++;
	  m_curOut[m_nEvent - 1][m_nHit[m_nEvent - 1]][0] = pix_col;
	  m_curOut[m_nEvent - 1][m_nHit[m_nEvent - 1]][1] = pix_row;
	  m_curOut[m_nEvent - 1][m_nHit[m_nEvent - 1]][2] = pix_tot;
#elif MAKEEVENT == 3
	  m_curOut->curEvent->addHitReserve(pix_row, pix_col, pix_tot);
#endif

#if XCHECK == 1
	  if(_debug >= 1) std::cout << "\n+++ Col: " << pix_col << ", Row: " << pix_row << ", Ccol: " << int(ccol - 1) << ", qRow: " << int(qrow) << ", ToT: " << int(pix_tot) << " +++\n" << std::endl;
	  // Cross-check with input data
	  std::tuple<uint16_t, uint16_t, uint8_t> hit = std::make_tuple(pix_col, pix_row, pix_tot);
	  auto it = std::find(_xcheckMap.begin(), _xcheckMap.end(), hit);
	  
	  if(it == _xcheckMap.end()){
	    std::cout << "Col: " << pix_col << ", Row: " << pix_row << ", Ccol: " << int(ccol - 1) << ", qRow: " << int(qrow) << ", ToT: " << int(pix_tot) 
		      << " cannot be found in xcheck data" << std::endl;
	    _cannotBeFound++;
	    getchar();
	  }
	  // else if(_xcheckMap[m_curOut->nEvents-1][hit] != pix_tot){
	  //   std::cout << "Col: " << pix_col << ", Row: " << pix_row << ", ToT: " << int(pix_tot) 
	  // 	      << " has different ToT value compared with xcheck data: " << int(_xcheckMap[m_curOut->nEvents-1][hit]) << std::endl; 
	  //   _wrongToT++;
	  //   getchar();
	  // }
	  else _xcheckMap.erase(it);
#endif
	}
      } while(!(islast_isneighbor_qrow_hmap & 0x200));
    }
  }
}
